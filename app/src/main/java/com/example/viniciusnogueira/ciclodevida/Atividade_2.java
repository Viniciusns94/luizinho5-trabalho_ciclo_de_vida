package com.example.viniciusnogueira.ciclodevida;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class Atividade_2 extends AppCompatActivity {

    public static final String TAG2 = "AT2 Parte do Código: ";

    public static final String CREATE = "CREATE", RESUME = "RESUME", START = "START", RESTART = "RESTART",
            PAUSE = "PAUSE", STOP = "STOP", DESTROY = "DESTROY";

    public int nCreate, nRestart, nStart, nResume, nPause, nStop, nDestroy;
    public TextView tCreate, tStart, tResume, tRestart, tPause, tStop, tDestroy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atividade_2);

        tCreate = (TextView) findViewById(R.id.tx_onCreate_at2);
        tRestart = (TextView) findViewById(R.id.tx_onRestart_at2);
        tStart = (TextView) findViewById(R.id.tx_onStart_at2);
        tResume = (TextView) findViewById(R.id.tx_onResume_at2);
        tPause = (TextView) findViewById(R.id.tx_onPause_at2);
        tStop = (TextView) findViewById(R.id.tx_onStop_at2);
        tDestroy = (TextView) findViewById(R.id.tx_onDestroy_at2);

        if(savedInstanceState != null){
            nCreate = savedInstanceState.getInt(CREATE);
            nRestart = savedInstanceState.getInt(RESTART);
            nStart = savedInstanceState.getInt(START);
            nResume = savedInstanceState.getInt(RESUME);
            nPause = savedInstanceState.getInt(PAUSE);
            nStop = savedInstanceState.getInt(STOP);
            nDestroy = savedInstanceState.getInt(DESTROY);
        }
        Log.i(TAG2, "Acessou onCreat at2 com SUCESSO!!");
        contagemAtualizada();
    }

    private void contagemAtualizada() {
        tCreate.setText("onCreat(): " + nCreate);
        tStart.setText("onStart(): " + nStart);
        tRestart.setText("onRestart(): " + nRestart);
        tResume.setText("onResume(): " + nResume);
        tPause.setText("onPause(): " + nPause);
        tStop.setText("onStop(): " + nStop);
        tDestroy.setText("onDestroy(): " + nDestroy);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG2, "Acessou onStart at2 com SUCESSO!!");
        nStart++;
        contagemAtualizada();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG2, "Acessou onRestart at2 com SUCESSO!!");
        nRestart++;
        contagemAtualizada();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG2, "Acessou onResume at2 com SUCESSO!!");
        nResume++;
        contagemAtualizada();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG2, "Acessou onPause at2 com SUCESSO!!");
        nPause++;
        contagemAtualizada();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG2, "Acessou onStop at2 com SUCESSO!!");
        nStop++;
        contagemAtualizada();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG2, "Acessou onDestroy at2 com SUCESSO!!");
        nDestroy++;
        contagemAtualizada();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(START, nStart);
        outState.putInt (RESUME, nResume);
        outState.putInt (RESTART, nRestart);
        outState.putInt(CREATE, nCreate);
        outState.putInt(PAUSE, nPause);
        outState.putInt(STOP, nStop);
        outState.putInt(DESTROY, nDestroy);
    }
}
