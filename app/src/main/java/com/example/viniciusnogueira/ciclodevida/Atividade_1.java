package com.example.viniciusnogueira.ciclodevida;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Atividade_1 extends AppCompatActivity {

    public static final String TAG = "AT1 Parte do Código: "; //psf tab

    public static final String CREATE = "CREATE", RESUME = "RESUME", START = "START", RESTART = "RESTART",
            PAUSE = "PAUSE", STOP = "STOP", DESTROY = "DESTROY";

    public int nCreate, nRestart, nStart, nResume, nPause, nStop, nDestroy;
    public TextView tCreate, tStart, tResume, tRestart, tPause, tStop, tDestroy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atividade_1);

        Button bt = (Button) findViewById(R.id.button_1);

        tCreate = (TextView) findViewById(R.id.tx_onCreate);
        tRestart = (TextView) findViewById(R.id.tx_onRestart);
        tStart = (TextView) findViewById(R.id.tx_onStart);
        tResume = (TextView) findViewById(R.id.tx_onResume);
        tPause = (TextView) findViewById(R.id.tx_onPause);
        tStop = (TextView) findViewById(R.id.tx_onStop);
        tDestroy = (TextView) findViewById(R.id.tx_onDestroy);

        if (bt != null) {
            bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Atividade_1.this, Atividade_2.class);
                    startActivity(intent);
                }
            });
        }

        if (null != savedInstanceState) {
            nCreate = savedInstanceState.getInt(CREATE);
            nStart = savedInstanceState.getInt(START);
            nRestart = savedInstanceState.getInt(RESTART);
            nResume = savedInstanceState.getInt(RESUME);
            nPause = savedInstanceState.getInt(PAUSE);
            nStop = savedInstanceState.getInt(STOP);
            nDestroy = savedInstanceState.getInt(DESTROY);
        }

        Log.i(TAG, "Acessou o onCreate com SUCESSO!!");
        nCreate++;
        atualizaContagem();
    }

    private void atualizaContagem() {
        tCreate.setText("onCreate(): " + nCreate);
        tStart.setText("onStart(): " + nStart);
        tRestart.setText("onRestart(): " + nRestart);
        tResume.setText("onResume(): " + nResume);
        tPause.setText("onPause(): " + nPause);
        tStop.setText("onStop(): " + nStop);
        tDestroy.setText("onDestroy(): " + nDestroy);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "Acessou o onStart com SUCESSO!!");
        nStart++;
        atualizaContagem();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "Acessou o onResume com SUCESSO!!");
        nResume++;
        atualizaContagem();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "Acessou o onRestart com SUCESSO!!");
        nRestart++;
        atualizaContagem();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "Acessou o onPause com SUCESSO!!");
        nPause++;
        atualizaContagem();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "Acessou o onStop com SUCESSO!!");
        nStop++;
        atualizaContagem();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Acessou o onDestroy com SUCESSO!!");
        nDestroy++;
        atualizaContagem();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(START, nStart);
        outState.putInt (RESUME, nResume);
        outState.putInt (RESTART, nRestart);
        outState.putInt (CREATE, nCreate);
        outState.putInt(PAUSE, nPause);
        outState.putInt(STOP, nStop);
        outState.putInt(DESTROY, nDestroy);
    }
}
